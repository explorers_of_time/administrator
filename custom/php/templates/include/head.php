
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93035021-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-93035021-4');
</script>


  <link rel="apple-touch-icon-precomposed" href="https://danielwaleczek.com/img/favicon_152.png">

<!---IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) ---->

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="https://danielwaleczek.com/img/favicon_144.png">

<!--- Replace #FFFFFF with your desired tile color. ---->

<!--- IE 11 Tile for Windows 8.1 Start Screen ---->

    <meta name="application-name" content="Name">
    <meta name="msapplication-tooltip" content="Tooltip">
    <meta name="msapplication-config" content="https://danielwaleczek.com/xml/ieconfig.xml">

  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
 
	
    
