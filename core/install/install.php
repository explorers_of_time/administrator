<?php
  require_once(__DIR__.'../../php/classes/validator.php');
	

  if (isset($_POST['install_button'])) {
    
    //TODO: extend validator class to handle db_name and db_prefix
    $v= new LoginValidator($_POST);
    $v->validate();
    $errs= $v->get_all_errors();

    if (empty($errs)) {
      //TODO: continue installing process - create db, tables and root, then proceed to installed.php and remove this file from server 
    }
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
 
	
   
  
		<title>Install content managament system</title>
  </head>
  
  <body>
    <div class="wrapper">
      
      <!--Banner-->
      <div class="container-fluid mx-0 my-0 py-0 px-0 text-center d-sm-block d-none">
		    <div class="row no-gutters">
			    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  black">
			      <img src="img/web_banner_1.3.png" alt="X-SiGMA" class="banner_image">
          </div>
		    </div>
      </div>
  
      <main class="container-fluid justify-content-center text-center px-4 py-4">
        <br><br>

        <h4>Welcome to X-SiGMA AdminSys Installer</h4>

        <br><br>

        <form>
          <p>Enter the databse name which will be used to store all of the AdminSys tables</p>
          <input class="form-control" id="db_name" name="db_name" placeholder="Database name"/>
          <div id="db_name_error" class="field_error">test</div>

          <br><br><br>

          <p>Enter tables prefix</p>
          <p>Leave the filed blank to use the system default prefix</p>
          <input class="form-control" id="tables_prefix" name="tables_prefix" placeholder="Default prefix &ldquo;xs&rdquo;"/>
          <div id="db_prefix_error" class="field_error">test</div>

          <br><br>

          <hr>

          <br><br>

          <h4>Create Super User account</h4>
          <p>This will be the system root account with full access to all the system resources</p>

          <br><br>

          <p>Enter Super User username</p>
          <input class="form-control" id="username" name="username" placeholder="Username"/>
          <div id="username_error" class="field_error">test</div>

          <br><br><br>

          <p>Enter Super User password</p>
          <input class="form-control" id="password" name="password" placeholder="Password"/>
          <div id="password_error" class="field_error">test</div>

          <br><br><br>

          <button id="install_button" name="install_button" type="button" class="btn btn-outline-primary">Install</button>
        </form>
      </main>

      <!-- Footer -->
  <footer class="page-footer font-small black py-4 px-1 sticky-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="order-2 order-md-1 col-12 col-sm-12 col-md-4  col-lg-4 col-xl-4 px-1 pt-3 text-center text-md-left copyright">
				Copyright&nbsp;&copy;2018-2019 X-SiGMA Systems. <br>
				All rights reserved.
			</div>
			<div class="order-1 order-md-2 col-12 col-sm-12 col-md-8  col-lg-8 col-xl-8 px-1 pt-4 text-center	text-md-right ">
				<a href="https://www.facebook.com/XSiGMA/" target="_blank" class="mx-2"><i class="fab fa-facebook fa-2x text-light"></i></a>
                <a href="https://twitter.com/xsigmasystems" target="_blank" class="mx-2"><i class="fab fa-twitter fa-2x text-light"></i></a>
				<a href="https://www.instagram.com/xsigmasystems/" target="_blank" class="mx-2"><i class="fab fa-instagram fa-2x text-light"></i></a>
                <a href="mailto:xsigmasystems@gmail.com" target="_blank" class="mx-2"><i class="far fa-envelope fa-2x text-light"></i></a>
			</div>
			
		</div>
	</div>
	


</footer>

    </div>

    <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="../../../../bootstrap/dist/css/bootstrap.min.css">
	
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/main_install.css">
	
	  <script src="https://kit.fontawesome.com/2694440e40.js"></script>
	
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
    <script src="../../../../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../../../../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>