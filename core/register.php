<?php 
session_start();

require_once('php/classes/hasher.php');
require_once('php/classes/validator.php');
require_once(__DIR__.'../../custom/config.php');


// set up PDO with values defined in config.php
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$username = "";
$errors   = array(); 


if (isset($_POST['register_button'])) {
	register();
}


function register(){
	
	global $conn, $errors, $username;


	$username    = !empty($_POST['username']) ? trim($_POST['username']) : null;
	$password_1  = !empty($_POST['password_1']) ? trim($_POST['password_1']) : null;
	$password_2  = !empty($_POST['password_2']) ? trim($_POST['password_2']) : null; 
	
/*	if ($password_1 != $password_2) {
		die("Passwords are not identical");
	} else { */
	
	$data = array(
    'username_field'  => $username,
    'password_field' => $password_1,
	'password_confirm_field' => $password_2
	);
	
	$validator = new LoginValidator($data);
	$validator->validate();
	$errors = $validator->get_all_errors();
	
	
	if (count($errors) == 0) {
		//
		$pass = new Hasher($password_1);
		$hash_pass = $pass->get_hashed_string($password_1);

		if (isset($_POST['user_type'])) { //if user is registered from admin panel they might be given admin status, by checking in the form (future feature)
			$user_type = 1; //set user as admin 
			$query = "INSERT INTO users (name, isAdmin, password)
					  VALUES (:username, :user_type, :hash_pass)";
			 $statement = $conn->prepare( $query );
			 $statement->bindValue( ":username", $username, PDO::PARAM_STR );
			 $statement->bindValue( ":user_type", $user_type, PDO::PARAM_INT );
			 $statement->bindValue( ":hash_pass", $hash_pass, PDO::PARAM_BIN );
			
			 $statement->execute();
			$_SESSION['success']  = "New user successfully created!!";
			header('location: index.php');
		}else{
			$user_type = 0; //set user as normal user
			$query = "INSERT INTO users (name, isAdmin, password)  
					  VALUES (:username, :user_type, :hash_pass)";
			 $statement = $conn->prepare( $query );
			 $statement->bindValue( ":username", $username, PDO::PARAM_STR );
			 $statement->bindValue( ":user_type", $user_type, PDO::PARAM_INT );
			 $statement->bindValue( ":hash_pass", $hash_pass, PDO::PARAM_STR );
			
			 $statement->execute();

			/* would allow to start normal user session
			$logged_in_user_id = $conn->lastInsertId();

			$_SESSION['user'] = getUserById($logged_in_user_id); 
			$_SESSION['success']  = "You are now logged in";
			*/
			header('location: index.php');				
		}
		 
	} else {
		display_error($errors);
		die();
	}
	
}


function getUserById($id){
	global $conn;
	$query = "SELECT * FROM users WHERE id=" . $id;
	$result = $conn->query($query);

	$user = $result->fetchAll(PDO::FETCH_ASSOC);
	return $user;
}



function display_error() {
	global $errors;

	if (count($errors) > 0){
		echo '<div class="error">';
			foreach ($errors as $error){
				echo $error .'<br>';
			}
		echo '</div>';
	}
}	

?>

<!DOCTYPE html>
<html>
<head>
	<?php include __DIR__.'../../custom/php/templates/include/head.php' ?> 
		<?php include 
	__DIR__.'../../custom/titles.php' ?> 
		<title><?php echo REGISTER ?></title>
</head>
<body>

<div class="wrapper">

<?php include __DIR__.'../../custom/php/templates/include/header.php' ?>

<main class="container-fluid justify-content-center text-center px-4 py-4">

<form method="post" action="register.php">
	
		<label>Username:</label><br>
		<input class="form-control" type="text" name="username" value="<?php echo $username; ?>">
	
		<label>Password:</label><br>
		<input class="form-control" type="password" name="password_1">
	
		<label>Confirm password:</label><br>
		<input class="form-control" type="password" name="password_2">
	<br>
		<button type="submit" class="btn btn-outline-primary" name="register_button">Sign up</button>
	
	<p>
		Already have an account? <a href="index.php">Sign in</a>
	</p>
</form>

</main>

<?php include __DIR__.'../../custom/php/templates/include/footer.php' ?>

</div>
 <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../custom/css/news.css">
	<link rel="stylesheet" href="../custom/css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../../../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../../../bootstrap/dist/js/bootstrap.min.js" ></script>

</body>
</html>