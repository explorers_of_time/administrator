DROP TABLE IF EXISTS users;
CREATE TABLE users
(
  id              smallint unsigned NOT NULL auto_increment,
  name           varchar(255) NOT NULL,
  password		binary(60) NOT NULL,
  isAdmin              tinyint NULL,

  PRIMARY KEY     (id)
);