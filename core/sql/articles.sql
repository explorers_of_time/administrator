DROP TABLE IF EXISTS articles;
CREATE TABLE articles
(
  id              smallint unsigned NOT NULL auto_increment,
  dateOfPublication date NOT NULL,                             
  headline           varchar(255) NOT NULL,
  WebP_img 			 varchar(255) NOT NULL,
  img 			 varchar(255) NOT NULL,  
  sneakPeak         text NOT NULL,                              
  fullContent         mediumtext NOT NULL,                       
  pinned              tinyint NULL,

  PRIMARY KEY     (id)
);

