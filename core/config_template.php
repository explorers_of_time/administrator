<?php
ini_set("display_errors", true); // set to false on live site
date_default_timezone_set("Europe/London");
define("DB_DSN", "mysql:host=localhost;dbname=example");
define("DB_USERNAME","root");
define("DB_PASSWORD", "root");
define("CLASS_PATH", __DIR__.'../../core/php/classes');
define("TEMPLATE_PATH", __DIR__.'../../core/php/templates');
define("HOMEPAGE_NUM_ARTICLES", 9);
define("NUMBER_OF_ARTICLES", 3);
require(CLASS_PATH . "/article.php");

function handleException( $exception) {
	echo "Sorry, a problem occurred. Please try later.";
	error_log( $exception->getMessage() );
}

set_exception_handler('handleException');
?>