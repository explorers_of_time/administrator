  <!doctype html>
<html lang="en">
<head>
		<?php include __DIR__.'../../../../../custom/php/templates/include/head.php' ?> 
		<?php include __DIR__.'../../../../../custom/titles.php' ?> 
	<title> <?php echo htmlspecialchars( $results['article']->headline )?>  <?php echo READ_ARTICLE ?></title>
</head>
  <body>
  <div class="wrapper">
 
	 <?php include __DIR__.'../../../../../custom/php/templates/include/header.php' ?>  

<main class="container-fluid content flex-column justify-content-center text-justify px-4 py-4">
	  
	   <h1 class="headline" ><?php echo htmlspecialchars( $results['article']->headline )?></h1>
	   
     <div class="intro" ><!--<img src="../img/<?/*php echo $results['article']->img */?> " alt=""  class="py-4 artimage" style="height: auto;">-->
	  <?php echo htmlspecialchars( $results['article']->sneakPeak )?></div><br>
      <div class="fullContent" ><?php echo $results['article']->fullContent?></div><br>
      <p class="pubDate">Published on <?php echo date('j F Y', $results['article']->dateOfPublication)?></p>

      

</main>
 
<?php include __DIR__.'../../../../../custom/php/templates/include/footer.php' ?>
<!-- Footer -->
	</div>

  <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../custom/css/news.css">
	<link rel="stylesheet" href="../custom/css/main.css">
	<link rel="stylesheet" href="../custom/css/readArticle.css">
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
       <script src="../../../../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../../../../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>