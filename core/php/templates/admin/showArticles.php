<!doctype html>
<html lang="en">
  <head>
  
		<?php include __DIR__.'../../../../../custom/php/templates/include/head.php' ?> 
		<?php include __DIR__.'../../../../../custom/titles.php' ?> 
		<title><?php echo SHOW_ARTICLES ?></title>
  </head>
  <body>
  <div class="wrapper">
  <?php include __DIR__.'../../../../../custom/php/templates/include/header_admin.php' ?>   
   

<main class="container-fluid  px-4 py-4">
 
      <div id="adminHeader">
        <h2>Admin Panel</h2>
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="index.php?action=signOut"?>Log out</a></p>
      </div>

      <h1>All Articles</h1>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>


<?php if ( isset( $results['statusMessage'] ) ) { ?>
        <div class="statusMessage"><?php echo $results['statusMessage'] ?></div>
<?php } ?>

      <table>
        <tr>
          <th>Publication Date</th>
          <th>Article</th>
        </tr>

<?php foreach ( $results['articles'] as $article ) { ?>

        <tr>
          <td>
            <a><?php echo date('j M Y', $article->dateOfPublication)?></a>
          </td>
          <td style="cursor:pointer;">
            <a style="color: #0000E0;" onclick="location='index.php?action=modifyArticle&amp;articleId=<?php echo $article->id?>'">
                <?php echo $article->headline?>
            </a>
          </td>
        </tr>

<?php } ?>

      </table>

      <p><?php echo $results['totalRows']?> article<?php echo ( $results['totalRows'] != 1 ) ? 's' : '' ?> in total.</p>

      <p><a href="index.php?action=addArticle">Add a New Article</a></p>
</main>
 
 
<?php include __DIR__.'../../../../../custom/php/templates/include/footer.php' ?>
<!-- Footer -->
	</div>

   <!-- Bootstrap CSS -->
 <link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../custom/css/news.css">
	<link rel="stylesheet" href="../custom/css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../../../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../../../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>