<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
		<?php include __DIR__.'../../../../../custom/php/templates/include/head.php' ?> 
		<?php include __DIR__.'../../../../../custom/titles.php' ?> 
		<title><?php echo htmlspecialchars( $results['article']->headline )?><?php echo MODIFY_ARTICLE ?></title>
  </head>
  <body>
  <div class="wrapper">
  <?php include __DIR__.'../../../../../custom/php/templates/include/header.php' ?>
   


<main class="container-fluid justify-content-center  px-4 py-4">
 <div id="adminHeader">
        <h2>Welcome</h2>
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="index.php?action=signOut"?>Log out</a></p>
      </div>

      <h1><?php echo $results['pageheadline']?></h1>

      <form action="index.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="articleId" value="<?php echo $results['article']->id ?>"/>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

 

     
            <label for="headline">Article title</label><br>
            <input class="form-control" type="text" name="headline" id="headline" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->headline )?>" />
          	<br>
			
			 <label for="img">WebP Image file</label><br>
            <input class="form-control" type="text" name="WebP_img" id="WebP_img" placeholder="Name and extension of WebP image from img directory " autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->WebP_img )?>" />
          	<br>
			
            <label for="img">Image file</label><br>
            <input class="form-control" type="text" name="img" id="img" placeholder="Name and extension of image from img directory " autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->img )?>" />
          	<br>
          	
          	
            <label for="sneakPeak">Article summary</label><br>
            <textarea class="form-control" name="sneakPeak" id="sneakPeak" placeholder="Brief description of the article" required maxlength="1000" ><?php echo htmlspecialchars( $results['article']->sneakPeak )?></textarea>
          	<br>
          	
            <label for="fullContent">Full article</label><br>
            <textarea class="form-control" name="fullContent" id="fullContent" placeholder="The HTML code of the article" required maxlength="100000" ><?php echo htmlspecialchars( $results['article']->fullContent )?></textarea>
            <br>

          
            <label for="dateOfPublication">Publication Date</label><br>
            <input class="form-control" type="date" name="dateOfPublication" id="dateOfPublication" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['article']->dateOfPublication ? date( "Y-m-d", $results['article']->dateOfPublication ) : "" ?>" />
          	<br>

            <div class="form-check">
				  <input type="hidden" name="pinned" value="0" />
                  <input type="checkbox" class="form-check-input" name="pinned" id="pinned" maxlength="5" value="1" <?php if ($results['article']->pinned == 1) echo "checked='checked'"; ?> > 
            
                  <label class="form-check-label" for="pinned" >Pin the article</label>
            </div>
            
     

  
        <div class="buttons">
          <input type="submit" class="btn btn-outline-dark   my-4 mx-4" name="saveChanges" value="Save Changes" /></button>
          <input type="submit" class="btn btn-outline-dark   my-4 mx-4" formnovalidate name="cancel" value="Cancel" /></button>
        </div>

      </form>

     

<?php if ( $results['article']->id ) { ?>
      <p><a href="index.php?action=removeArticle&amp;articleId=<?php echo $results['article']->id ?>" onclick="return confirm('Delete This Article?')">Delete This Article</a></p>
<?php } ?>

</main>
 
<?php include __DIR__.'../../../../../custom/php/templates/include/footer.php' ?>
<!-- Footer -->
  </div>
  
  <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="../../../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../custom/css/news.css">
	<link rel="stylesheet" href="../custom/css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../../../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../../../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>