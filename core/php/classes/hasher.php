<?php
/**
 * Copyright ©2020 X-SiGMA System. All rights reserved.
 * 
 * The purpose of this class is to hash any string of characters using PASSWORD_BCRYPT algorithm.
 * 
 * @param $string Plain, unhashed string of characters
 * @param $hashed_string Hashed string
 * 
 * Created on 2/8/2020.
 * @author Michael Polowczuk
 */
class Hasher {
    
    private $string;
    private $hashed_string;
    

    /**
     * Public constructor.
     * Invokes hashing method.
     * 
     * @param $string Unhashed string value
     */
    public function __construct($string) {
        $this->string= $string;
        $this->hashed_string= $this->hash_string($string);
    }
    

    /**
     * Hashes given string of characters using built-in bCrypt algorithm.
     * 
     * @param $string Unhashed string value
     * @return $string Hashed string value
     */
    private function hash_string($string) {
        return password_hash($string, PASSWORD_BCRYPT);
    }
    
    
    /**
     * Sets $hashed_string property value.
     * 
     * @param $hashed_string Hashed string value
     */
    public function set_hashed_string($hashed_string) {
        $this->hashed_string= $hashed_string;
    }

    /**
     * Returns hashed string value.
     */
    public function get_hashed_string() {
        return $this->hashed_string;
    }

    /**
     * Sets $string property value.
     * 
     * @param $string plain/unhashed value
     */
    public function set_string($string) {
        $this->string= $string;
    }
    
    
    /**
     * Unsets variable storing plain/unhashed value.
     */
    public function __destruct() { // changed from protected
        unset($string);
    }
}

?>