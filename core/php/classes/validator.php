<?php
    /**
     * Copyright ©2020 X-SiGMA System. All rights reserved.
     * 
     * This class checks the validity of login data: Username and Password.
     * 
     * If any of the rules are not satisfied, an array of errors will be returned.
     * In this array, as keys for error messages, field names are being used.
     * 
     * The $password property is destroyed/unset for security purposes, after the script execution.
     * 
     * Created on 2/10/2020.
     * @author Michael Polowczuk
	 * @author Krystian Stebel
     */

     class LoginValidator {

        private $username;
        private $password_1;
		private $password_2;
        private $errors= [];


        public function __construct($data = array('username_field'=>"", 'password_field'=>"", 'password_confirm_field'=> NULL)) {
            $this->username= $data['username_field'];
            $this->password_1= $data['password_field'];
			$this->password_2 = $data['password_confirm_field'];
        }


        public function validate() {
            $this->validate_username();
            $this->validate_password();
        }


        private function validate_username() {
			
            if (empty($this->username) || $this->username == "" || strlen($this->username) == 0) {
                $this->add_error("username", "Username cannot be empty");
            }
            else if (strlen($this->username) < 3) {
                $this->add_error("username", "Username must be at least 3 characters long");
            }
            else if (strlen($this->username) > 20) {
                $this->add_error("username", "Username can be max 20 characters long");
            }
            else if (!preg_match('/^[a-zA-Z0-9]*$/', $this->username)) {
                $this->add_error("username", "Username must only contains letters and numbers");
            }
        }

        private function validate_password() {
            if (empty($this->password_1) || $this->password_1 == "" || strlen($this->password_1) == 0) {
                $this->add_error("password", "Password cannot be empty");
            }
            else if (strlen($this->password_1) < 6) {
                $this->add_error("password", "Password must be at least 6 characters long");
            }
            else if (strlen($this->password_1) > 254) {
                $this->add_error("password", "Password can be max 254 characters long");
            }
            else if (!preg_match('/[a-z]/', $this->password_1)) {
                $this->add_error("password", "Password must at least one lowercase letter");
            }
            else if (!preg_match('/[A-Z]/', $this->password_1)) {
                $this->add_error("password", "Password must at least one uppercase letter");
            }
            else if (!preg_match('/[0-9]/', $this->password_1)) {
                $this->add_error("password", "Password must at least one number letter");
            }
            else if($this->password_2 != NULL && $this->password_1 != $this->password_2) {
                $this->add_error("password", "Passwords do not match");
			}
            else if ($this->username == $this->password_1) {
                $this->add_error("password", "Password must be different from Username");
            }
        }


        private function add_error($key, $value) {
            $this->errors[$key]= $value;
        }

        public function get_error($key) {
            return $this->errors[$key];
        }

        public function get_all_errors() {
            return $this->errors;
        }


        function __destruct() {
            unset($this->password_1);
			unset($this->password_2);
        }
     }
?>