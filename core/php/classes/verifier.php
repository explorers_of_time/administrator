<?php
    /**
     * Copyright ©2020 X-SiGMA System. All rights reserved.
     * 
     * This class verifies if the password inserted by a user in login form
     * matches the one stored in a database/config file.
     * Then returns either true or false if passwords matche or are different respectively. 
     * 
     * The class requires two arguments to be passed in its constructor:
     *  -> $db_password     -   which is a password fetched from a database
     *  -> $login_password  -   password inserted by the user in login form
     * 
     * The $db_password & $login_password properties are destroyed/unset for security purposes,
     * after the script execution.
     * 
     * Created on 2/8/2020.
     * @author Michael Polowczuk
     */
    class Verifier {

        private $db_password;
        private $login_password;
        private $verified= false;


        /**
         * Default constructor.
         * Sets class-global properties and calls verify method.
         * 
         * @param $password Password fetched from a database
         */
        public function __construct($password, $string) {
            $this->db_password= $password;
            $this->login_password= $string;

            $this->verify_password();
        }


        /**
         * Function verifies the password given by a user with the password featched from a database,
         * using PHP built-in function "password_verify()".
         * 
         * Then changes $verified property to either true or false.
         */
        private function verify_password() {
            
            if (password_verify($this->login_password, $this->db_password)) {
                $this->verified= true;
            }
            else {
                $this->verified= false;
            }
        }

        /**
         * Returns the result of verification:
         *  -> true   -   if passwords match
         *  -> false  -   if given password is different to on stored in a database
         */
        public function get_verified() {
            return $this->verified;
        }


        /**
         * Unsets $db_password and $plain_password variables.
         */
        protected function __destruct() {
            unset($db_password);
            unset($plain_password);
        }
    }
?>