<?php
    /**
     * Copyright ©2020 X-SiGMA System. All rights reserved.
     * 
     * This class stores information data required to:
     *  - create database
     *  - create tables within database
     *  - create Super User (root)
     * 
     * Created on 3/5/2020.
     * @author Michael Polowczuk
     */

     class Installer {
         
        private $db_name;
        private $db_prefix;
        private $User;


        public function __construct() {
            $this->User= new User();
        }


        public function set_db_name($db_name) {
            $this->db_name= $db_name;
        }

        public function get_db_name() {
            return $this->db_name;
        }

        public function set_db_prefix($db_prefix) {
            $this->db_prefix= $db_prefix;
        }

        public function get_db_prefix() {
            return $this->db_prefix;
        }

        public function set_User($User) {
            $this->User= $User;
        }

        public function get_User() {
            return $this->User;
        }
     }
?>