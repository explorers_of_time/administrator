<?php
    /**
     * Copyright ©2020 X-SiGMA System. All rights reserved.
     * 
     * This class stores information about a user.
     * 
     * Created on 2/8/2020.
     * @author Michael Polowczuk
     */
    class User {
        
        private $username;
        private $password;
        
        
        /**
         * Sets username property.
         * 
         * @param $username Username string value
         */
        public function set_username($username) {
            $this->username = $username;
        }
        
        /**
         * Returns $username property value.
         * 
         * @return $username value
         */
        public function get_username() {
            return $this->username;
        }
        
        /**
         * Sets $password property value.
         * 
         * @param $password value
         */
        public function set_password($password) {
            $this->password = $password;
        }
        
        /**
         * Returns $password property value.
         * 
         * @return $password value
         */
        public function get_password() {
            return $this->password;
        }
    }

?>